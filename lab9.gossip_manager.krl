ruleset lab9.gossip_manager {
  meta {
    shares __testing, children, sensors
    use module io.picolabs.wrangler alias wrangler
  }
  global {
    __testing = { "queries":
      [ { "name": "__testing" },
        {"name": "children"},
        {"name": "sensors"}
      //, { "name": "entry", "args": [ "key" ] }
      ] , "events":
      [ 
        { "domain": "gossip_manager", "type": "create" },
        {"domain": "gossip_manager", "type": "add_message", "attrs": ["node", "sensorId", "sequenceNumber", "temperature"]},
        {"domain": "gossip_test", "type": "start_all"},
        {"domain": "gossip_test", "type": "stop_all"},
        {"domain": "gossip_manager", "type": "delete"},
        {"domain": "gossip_manager", "type": "create_child", "attrs": ["name"]}
      ]
    };
    
    children = function() {
      ent:children;
    };
    sensors = function() {
      // randomly generate three sensor ids, if not already created, and return the next sequence number
      ent:sensors.defaultsTo({});
    };
    numSensors = function() {
      sensors().length();
    }
    starting = function() {
      ent:starting.defaultsTo(false);
    }
  }
  
  rule create_nodes {
    select when gossip_manager create
    pre {
      correlationId = random:uuid();
      updatedCorrelators = {}.put(correlationId, 0);
    }
    noop()
    fired {
      raise wrangler event "child_creation"
        attributes { "name": "gossip1",
                     "color": "#ff1d19",
                     "correlationId": correlationId,
                     "rids": ["io.picolabs.logging", "lab9.gossip"]};
      raise wrangler event "child_creation"
        attributes { "name": "gossip2",
                     "color": "#ff2d19",
                     "correlationId": correlationId,
                     "rids": ["io.picolabs.logging", "lab9.gossip"]};
      raise wrangler event "child_creation"
        attributes { "name": "gossip3",
                     "color": "#ff3d19",
                     "correlationId": correlationId,
                     "rids": ["io.picolabs.logging", "lab9.gossip"]};
      raise wrangler event "child_creation"
        attributes { "name": "gossip4",
                     "color": "#ff4d19",
                     "correlationId": correlationId,
                     "rids": ["io.picolabs.logging", "lab9.gossip"]};
      raise wrangler event "child_creation"
        attributes { "name": "gossip5",
                     "color": "#ff5d19",
                     "correlationId": correlationId,
                     "rids": ["io.picolabs.logging", "lab9.gossip"]};
      ent:correlationIds := updatedCorrelators;
      ent:starting := true;
    }
  }
  
  rule collect_initialized {
    select when wrangler child_initialized where starting() == true
    pre {
      correlationId = event:attr("rs_attrs"){"correlationId"};
      count = ent:correlationIds{correlationId} + 1;
      b = count.klog("count:");
      
      updatedCorrelators = ent:correlationIds.put(correlationId, count);
    }
    if count >= 5 then
      noop()
    fired {
      raise gossip_manager event "create_network";
    } finally {
      ent:correlationIds := updatedCorrelators;
    }
  }
  
  rule network_nodes {
    select when gossip_manager create_network
    // setup the network
    pre {
      children = wrangler:children().reduce(function(a, b) {a.put(b{"name"}, b)}, {}).klog("children:");
      rxs = children.map(function(child) {
        {}.put(child{"name"}, 
            wrangler:skyQuery(child{"eci"}, "io.picolabs.wrangler", "channel", {"id": "wellKnown_Rx"})
                .filter(function(channel) {channel{"name"} == "wellKnown_Rx"}).head(){"id"});
      }).reduce(function(a, b) {a.put(b)}, {})
      .map(function(value, key) {value{key}}).klog("children wellKnown_Rxs:");
    }
    every {
      event:send({"eci": children{"gossip1"}{"eci"}, "eid": 0, "domain": "gossip", "type": "add_peer",
            "attrs": {"Tx": rxs{"gossip2"}}});
      event:send({"eci": children{"gossip1"}{"eci"}, "eid": 0, "domain": "gossip", "type": "add_peer",
            "attrs": {"Tx": rxs{"gossip3"}}});
      event:send({"eci": children{"gossip1"}{"eci"}, "eid": 0, "domain": "gossip", "type": "add_peer",
            "attrs": {"Tx": rxs{"gossip4"}}});
      event:send({"eci": children{"gossip2"}{"eci"}, "eid": 0, "domain": "gossip", "type": "add_peer",
            "attrs": {"Tx": rxs{"gossip3"}}});
      event:send({"eci": children{"gossip4"}{"eci"}, "eid": 0, "domain": "gossip", "type": "add_peer",
            "attrs": {"Tx": rxs{"gossip5"}}});
    }
    always {
      ent:children := children;
      ent:sensors := {}
        .put(random:uuid(), -1)
        .put(random:uuid(), -1)
        .put(random:uuid(), -1);
      ent:starting := false;
    }
  }
  
  rule add_message {
    select when gossip_manager add_message
    pre {
      sensorId = event:attr("sensorId") && event:attr("sensorId") != "" => 
          event:attr("sensorId").klog("sensorId:") |
          sensors().keys()[random:integer(numSensors() - 1)].klog("sensorId:");
          
      sequenceNum = event:attr("sequenceNumber") && event:attr("sequenceNumber") != "" => 
          event:attr("sequenceNumber").klog("sequenceNumber:") |
          (sensors(){sensorId} + 1).klog("sequenceNumber:");
          
      temperature = event:attr("temperature") && event:attr("temperature") != "" => 
          event:attr("temperature").klog("temperature:") |
          (random:integer(45) + 35).klog("temperature:");
          
      node = event:attr("node") && event:attr("node") != "" => 
          event:attr("node").klog("node:") |
          ent:children.keys()[random:integer(ent:children.keys().length() - 1)].klog("node:");
          
      attrs = {"sensorId": sensorId, "sequenceNumber": sequenceNum, "temperature": temperature};
      
      modifiedSensors = sensors().put(sensorId, sequenceNum);
    }
    every {
      event:send({
        "eci": ent:children{node}{"eci"},
        "eid": 0,
        "domain": "gossip_test",
        "type": "add_message",
        "attrs": attrs});
      send_directive("added_message", attrs.put("node", node));
    }
    fired {
      ent:sensors := modifiedSensors;
    }
  }
  
  rule delete_children {
    select when gossip_manager delete
      foreach ent:children setting (child)
    always {
      raise wrangler event "child_deletion"
        attributes {"name": child{"name"}};
      ent:children := {} on final;
      ent:sensors := null on final;
    }
  }
  
  rule start_all_heartbeats {
    select when gossip_test start_all
      foreach ent:children.values() setting (node)
    pre {
      b = node{"name"}.klog("starting heartbeat for node:");
    }
    event:send({
      "eci": node{"eci"}, 
      "eid": 0,
      "domain": "gossip", 
      "type": "heartbeat"});
  }
  
  rule stop_all_heartbeats {
    select when gossip_test stop_all
      foreach ent:children.values() setting (node)
    pre {
      b = node{"name"}.klog("stopping heartbeat for node:");
    }
    event:send({
      "eci": node{"eci"}, 
      "eid": 0,
      "domain": "gossip_test", 
      "type": "stop_heartbeat"});
  }
  
  rule create_child {
    select when gossip_manager create_child
    pre {
      nodeName = event:attr("name");
    }
    noop();
    fired {
      raise wrangler event "child_creation"
        attributes { "name": nodeName,
                     "color": "#c60d19",
                     "rids": ["io.picolabs.logging", "lab9.gossip"]};
    }
  }
  
  rule child_initialized {
    select when wrangler child_initialized where starting() == false
    pre {
      children = wrangler:children().reduce(function(a, b) {a.put(b{"name"}, b)}, {}).klog("children:");
      rxs = children.map(function(child) {
        {}.put(child{"name"}, 
            wrangler:skyQuery(child{"eci"}, "io.picolabs.wrangler", "channel", {"id": "wellKnown_Rx"})
                .filter(function(channel) {channel{"name"} == "wellKnown_Rx"}).head(){"id"});
      }).reduce(function(a, b) {a.put(b)}, {})
      .map(function(value, key) {value{key}}).klog("children wellKnown_Rxs:");
      otherRxs = rxs.filter(function(value, key) {
        key != event:attr("name")
      }).klog("other Rxs (potential subscribers):");
      randomPeer = otherRxs.keys()[random:integer(otherRxs.keys().length() - 1)]
    }
    event:send({"eci": children{event:attr("name")}{"eci"}, "eid": 0, "domain": "gossip", "type": "add_peer",
          "attrs": {"Tx": rxs{randomPeer}}});
    fired {
      ent:children := children;
    }
  }
}
