ruleset lab7.manager_profile {
  meta {
    shares __testing
    use module manage_sensors
    use module lab2_keys
    use module twilio
        with account_sid = keys:twilio_keys{"account_sid"}
            auth_token =  keys:twilio_keys{"auth_token"}
  }
  global {
    __testing = { "queries":
      [ { "name": "__testing" }
      ] , "events":
      [
        {"domain": "sensor", "type": "profile_updated",
          "attrs": ["name", "location", "threshold", "sms"]}
      ]
    }
    
    sms = function() {
      ent:sms_number.defaultsTo(manage_sensors:default_sms())
    };
    
    phone_from = "+13853233201"
  }
  
  rule update_profile {
    select when manager profile_updated
    // we can't return the updated values in the response because entity
    //   variables can only be updated in the postlude
    send_directive("update profile", {"message": "processed"});
    always {
      ent:sms_number := event:attr("sms").defaultsTo(sms());
    }
  }
  
  /**
   * Rule that is raised to send an sms notification to the stored sms number,
   * typically when there's a temperature threshold violation (the threshold
   * is stored by the sensor pico).
   */
  rule sms_notification {
    select when manager sms_notification
    twilio:send_sms(sms(), phone_from, event:attr("message"))
  }
  
  /**
   * Raised by subscribed sensors whenever there is a threshold violation event.
   */
  rule threshold_violation {
    select when sensor threshold_violation
    always {
      raise manager event "sms_notification"
        attributes {"message":"high temperature violation: " 
          + event:attr("temperature") + " over threshold: " 
          + event:attr("threshold")};
    }
  }
}
